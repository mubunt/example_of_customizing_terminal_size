# RELEASE NOTES: *example_of_customizing_terminal_size*, Example of customizing the terminal size.

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 1.0.1**:
  - Removed unused make file.

**Version 1.0.0**:
  - First version.
