//------------------------------------------------------------------------------
// Copyright (c) 2022, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: example_of_customizing_terminal_size
// Example of customizing the terminal size
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termcap.h>
#include <termios.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <X11/Xlib.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "example_of_customizing_terminal_size_cmdline.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define PREFIX_FATAL		"FATAL ERROR: "
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _error( const char *label ) {
	fprintf(stderr, "\n" PREFIX_FATAL "%s.\n\n", label);
	exit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
/*
Returns the parent window of "window" (i.e. the ancestor of window
that is a direct child of the root, or window itself if it is a direct child).
If window is the root window, returns window.
*/
static Window get_toplevel_parent(Display *display, Window window) {
	Window parent;
	Window root;
	Window *children;
	unsigned int num_children;

	while (1) {
		if (0 == XQueryTree(display, window, &root, &parent, &children, &num_children))
			_error("XQueryTree error");
		if (children) XFree(children);
		if (window == root || parent == root) return window;
		window = parent;
	}
}
//------------------------------------------------------------------------------
static Window _get_display_and_window( Display **display ) {
	*display = XOpenDisplay(NULL);
	if (*display == NULL)
		_error("Cannot connect to X server");
	Window focus;
	int revert;
	XGetInputFocus(*display, &focus, &revert);
	return get_toplevel_parent(*display, focus);
}
//------------------------------------------------------------------------------
static void _get_window_size( Display *display, Window window, int *window_width, int *window_height ) {
	XWindowAttributes window_attributes;
	XGetWindowAttributes(display, window, &window_attributes);
	*window_width = window_attributes.width;
	*window_height = window_attributes.height;
}
//------------------------------------------------------------------------------
static void _set_window_size( Display *display, Window window, unsigned int window_width, unsigned int window_height ) {
	XResizeWindow(display, window, window_width, window_height);
}
//------------------------------------------------------------------------------
static void _get_terminal_size( int *lines, int *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w))
		_error("Cannot get terminal size");
	*lines = w.ws_row;
	*cols = w.ws_col;
}
//------------------------------------------------------------------------------
static void _set_terminal_size( int lines, int cols ) {
	struct winsize w;
	w.ws_row = (short unsigned) lines;
	w.ws_col = (short unsigned) cols;
	if (0 != ioctl(STDOUT_FILENO, TIOCSWINSZ, &w))
		_error("Cannot set terminal size");
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_example_of_customizing_terminal_size(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	int desired_number_of_cols = args_info.cols_arg;
	if (desired_number_of_cols <= 0) {
		cmdline_parser_example_of_customizing_terminal_size_free(&args_info);
		_error("Wrong number of cols");
	}
	bool verbose = (bool) args_info.verbose_given;
	cmdline_parser_example_of_customizing_terminal_size_free(&args_info);
	//----  Go on --------------------------------------------------------------
	if (verbose) {
		int i;
		for (i = 0; i < desired_number_of_cols; i++) {
			if (i % 10) fprintf(stdout, " ");
			else fprintf(stdout, "%1d", i / 10);
		}
		fprintf(stdout, "\n");
		for (i = 0; i < desired_number_of_cols; i++)
			fprintf(stdout, "%1d", i % 10);
		fprintf(stdout, "\n");
	}

	int lines, cols;
	_get_terminal_size(&lines, &cols);
	if (verbose) fprintf(stdout, "INITIAL TERMINAL SIZE: cols=%d - lines=%d\n", cols, lines);
	Display *display;
	Window window = _get_display_and_window(&display);
	int width, height;
	_get_window_size(display, window, &width, &height);
	if (verbose) fprintf(stdout, "INITIAL WINDOW SIZE: width=%d - height=%d\n", width, height);
	XSelectInput(display, window, StructureNotifyMask);
	int nwidth = width + ((desired_number_of_cols - cols) * (width / cols));
	_set_window_size(display, window, (unsigned int) nwidth, (unsigned int) height);
	if (verbose) fprintf(stdout, "SET WINDOW SIZE TO width=%d, roughly %d characters\n", nwidth, desired_number_of_cols);
	_get_window_size(display, window, &width, &height);
	_set_terminal_size(lines, desired_number_of_cols);
	_get_terminal_size(&lines, &cols);
	if (verbose)  fprintf(stdout, "FINAL TERMINAL SIZE: cols=%d - lines=%d\n", cols, lines);
	//---- Exit ----------------------------------------------------------------
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
