 # *example_of_customizing_terminal_size*, Example of customizing the terminal size.

This application is a functional example of how to programmatically get and change the number of columns of the current terminal (xterm like). 

## LICENSE
**example_of_customizing_terminal_size** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ example_of_customizing_terminal_size --help
Usage: example_of_customizing_terminal_size [OPTION]... [FILE]...
Example of customizing the terminal size

  -h, --help        Print help and exit
  -V, --version     Print version and exit
  -c, --cols=SHORT  Desired number of columns
  -v, --verbose     Verbose mode  (default=off)

Exit: returns a non-zero status if an error is detected.

$ example_of_customizing_terminal_size -V
example_of_customizing_terminal_size - Copyright (c) 2022, Michel RIZZO. All Rights Reserved.
example_of_customizing_terminal_size - Version 1.0.0

$ example_of_customizing_terminal_size --cols=100 --verbose
0         1         2         3         4         5         6         7         8         9         
0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
INITIAL TERMINAL SIZE: cols=120 - lines=60
INITIAL WINDOW SIZE: width=859 - height=958
SET WINDOW SIZE TO width=719, roughly 100 characters
FINAL TERMINAL SIZE: cols=100 - lines=60

$

```
## STRUCTURE OF THE APPLICATION
This section walks you through **example_of_customizing_terminal_size**'s structure. Once you understand this structure, you will easily find your way around in **example_of_customizing_terminal_size**'s code base.

``` bash
$ yaTree
./                                               # Application level
├── src/                                         # Source directory
│   ├── Makefile                                 # Makefile
│   ├── example_of_customizing_terminal_size.c   # Main C source file
│   └── example_of_customizing_terminal_size.ggo # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md                                   # GNU General Public License markdown file
├── LICENSE.md                                   # License markdown file
├── Makefile                                     # Makefile
├── README.md                                    # ReadMe markdown file
├── RELEASENOTES.md                              # Release Notes markdown file
└── VERSION                                      # Version identification text file

1 directories, 9 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd example_of_customizing_terminal_size
$ make clean all
```

## SOFTWARE REQUIREMENTS
- For usage , nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.23.
- Developped and tested on XUBUNTU 22.04, GCC version 11.2.0 (Ubuntu 11.2.0-19ubuntu1), GNU Make 4.3

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***