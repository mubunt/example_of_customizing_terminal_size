#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	Top level makefile for C projects
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#		LANGUAGE		Processing option for language, if any
#
# 	MAKE VARIABLES:
#		COPYRIGHT 		Year(s) for copyright clause
#		DIRS 			List of source directories
#------------------------------------------------------------------------------
MUTE			= @
OPTIM_DEBUG		= -O0 -g
OPTIM_RELEASE	= -O2
STRIP_DEBUG		= :
STRIP_RELEASE	= strip
OPTIM 			= "$(OPTIM_DEBUG)"
STRIP 			= $(STRIP_DEBUG)
#-------------------------------------------------------------------------------
VERSION			= $(shell cat VERSION)
TAG 			= "Version $(VERSION) - $(shell LC_ALL=en_EN.UTF-8 LANG=en_EN.utf8 date '+%B %Y')"
GIT 			= git
#-------------------------------------------------------------------------------
include .make/help.mk
#-------------------------------------------------------------------------------
all clean install cleaninstall astyle cppcheck:
	$(MUTE)for dir in $(DIRS); do \
		echo "------------------------------------------------------------ $$dir"; \
		cd $$dir; $(MAKE) --no-print-directory $@ MUTE=$(MUTE) VERSION=$(VERSION) COPYRIGHT=$(COPYRIGHT) OPTIM=$(OPTIM) STRIP=$(STRIP) LANGUAGE=$(LANGUAGE); cd ..; \
	done
release:
	$(MUTE)$(MAKE) --no-print-directory clean
	$(MUTE)$(MAKE) --no-print-directory all OPTIM="$(OPTIM_RELEASE)" STRIP=$(STRIP_RELEASE)
	$(MUTE)$(MAKE) --no-print-directory cleaninstall
	$(MUTE)$(MAKE) --no-print-directory install
	$(MUTE)$(MAKE) --no-print-directory clean
commit:
	$(GIT) commit -m $(TAG)
	$(GIT) tag -a v$(VERSION) -m $(TAG)
	$(GIT) push
	$(GIT) push --tags
#-------------------------------------------------------------------------------
.PHONY: all clean install cleaninstall astyle cppcheck commit release help
#-------------------------------------------------------------------------------
